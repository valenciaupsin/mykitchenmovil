package com.sprint.mykitchen.APIServices;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIAdapter {

    private static Services API_SERVICE;
    private static Services API_SERVICE2;


    public static Services getApiService(String token){

//        String token = "S<yus%;|ZO'1k/ISa^H+6_,!:&$0Z+kM9)B?;f`=]=p%q!)uJ^x_!F!7!LL&F|B";
//        String token = "S";

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(15,TimeUnit.SECONDS).addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
                        //api.mykitchen.com.mx:5000
                        //http://3.219.6.57:5000
        String baseUrl = "http://api.mykitchen.com.mx:5000/";
        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client) // <-- usamos el log level
                    .build();
            API_SERVICE = retrofit.create(Services.class);
        }

        return API_SERVICE;
    }
    public static Services getApiService2(String token){
        API_SERVICE2 = null;

//        String token = "S<yus%;|ZO'1k/ISa^H+6_,!:&$0Z+kM9)B?;f`=]=p%q!)uJ^x_!F!7!LL&F|B";
//        String token = "S";

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(15,TimeUnit.SECONDS).addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer "+token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        //api.mykitchen.com.mx:5000
        //http://3.219.6.57:5000
        String baseUrl = "http://api.mykitchen.com.mx:5000/";
        if (API_SERVICE2 == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client) // <-- usamos el log level
                    .build();
            API_SERVICE2 = retrofit.create(Services.class);
        }

        return API_SERVICE2;
//        return retrofit.create(Services.class);
    }
}
