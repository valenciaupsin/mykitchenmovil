package com.sprint.mykitchen.APIServices.response;

import com.sprint.mykitchen.inventory.model.Account;

public class LoginResponse {
    private String token;
    private Account account;
    private String kitchenId;

    public String getToken() {
        return token;
    }

    public Account getAccount() {
        return account;
    }

    public String getKitchenId() {
        return kitchenId;
    }
}
