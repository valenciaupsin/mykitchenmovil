package com.sprint.mykitchen.APIServices.response;

import com.sprint.mykitchen.ExplorarActivity.model.RecipeShort;

import java.util.ArrayList;

public class RecipesResponse {
    private String details;
    private ArrayList<RecipeShort> content;


    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public ArrayList<RecipeShort> getContent() {
        return content;
    }

    public void setContent(ArrayList<RecipeShort> content) {
        this.content = content;
    }
}
