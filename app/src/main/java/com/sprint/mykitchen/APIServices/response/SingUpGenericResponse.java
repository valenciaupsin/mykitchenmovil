package com.sprint.mykitchen.APIServices.response;

import com.sprint.mykitchen.RegistroActivity.NewUser;

public class SingUpGenericResponse {
    private String details;
    private NewUser content;

    public String getDetails() {
        return details;
    }

    public NewUser getContent() {
        return content;
    }
}
