package com.sprint.mykitchen.APIServices.response;

import com.sprint.mykitchen.ExplorarActivity.model.Product;

import java.util.ArrayList;

public class ProductsResponse {
    private String details;
    private ArrayList<Product> content;

    public String getDetails() {
        return details;
    }

    public ArrayList<Product> getContent() {
        return content;
    }
}
