package com.sprint.mykitchen.APIServices.response;

import com.sprint.mykitchen.inventory.model.Account;

import java.util.ArrayList;

public class LoginGenericResponse {
    private String detail;
    private LoginResponse content;

    public String getDetail() {
        return detail;
    }

    public LoginResponse getContent() {
        return content;
    }
}
