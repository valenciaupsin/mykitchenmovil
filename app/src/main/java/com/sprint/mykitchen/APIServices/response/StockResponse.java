package com.sprint.mykitchen.APIServices.response;


import com.sprint.mykitchen.inventory.model.Stock;

import java.util.ArrayList;

public class StockResponse {
    private String details;
    private ArrayList<Stock> content;

    public String getDetails() {
        return details;
    }

    public ArrayList<Stock> getContent() {
        return content;
    }
}
