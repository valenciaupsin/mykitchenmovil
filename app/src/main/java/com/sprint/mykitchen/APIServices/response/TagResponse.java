package com.sprint.mykitchen.APIServices.response;

import com.sprint.mykitchen.ExplorarActivity.model.Tag;

import java.util.ArrayList;

public class TagResponse {

    private String details;
    private ArrayList<Tag> content;

    public TagResponse(String details, ArrayList<Tag> content) {
        this.details = details;
        this.content = content;
    }

    public TagResponse() {
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public ArrayList<Tag> getContent() {
        return content;
    }

    public void setContent(ArrayList<Tag> content) {
        this.content = content;
    }
}
