package com.sprint.mykitchen.APIServices.response;

import com.sprint.mykitchen.ExplorarActivity.model.ProductCategory;

import java.util.ArrayList;

public class ProductCategoryResponse {
    private String details;
    private ArrayList<ProductCategory> content;

    public String getDetails() {
        return details;
    }

    public ArrayList<ProductCategory> getContent() {
        return content;
    }
}
