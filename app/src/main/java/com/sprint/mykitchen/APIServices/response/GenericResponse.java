package com.sprint.mykitchen.APIServices.response;

import com.sprint.mykitchen.inventory.model.StockText;

public class GenericResponse {
    private String details;
    private StockText content;

    public String getDetails() {
        return details;
    }

    public StockText getContent() {
        return content;
    }
}
