package com.sprint.mykitchen.APIServices.response;



import com.sprint.mykitchen.RecipeActivity.model.RecipeDetail;

import java.util.ArrayList;

public class RecipeDetailResponse {

    private String details;
    private RecipeDetail content;

    public RecipeDetailResponse() {
    }

    public RecipeDetailResponse(String details, RecipeDetail content) {
        this.details = details;
        this.content = content;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public RecipeDetail getContent() {
        return content;
    }

    public void setContent(RecipeDetail content) {
        this.content = content;
    }
}
