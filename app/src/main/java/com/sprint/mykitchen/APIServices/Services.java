package com.sprint.mykitchen.APIServices;

import com.sprint.mykitchen.APIServices.response.GenericResponse;
import com.sprint.mykitchen.APIServices.response.Login;
import com.sprint.mykitchen.APIServices.response.LoginGenericResponse;
import com.sprint.mykitchen.APIServices.response.ProductCategoryResponse;
import com.sprint.mykitchen.APIServices.response.ProductsResponse;
import com.sprint.mykitchen.APIServices.response.RecipesResponse;
import com.sprint.mykitchen.APIServices.response.SingUpGenericResponse;
import com.sprint.mykitchen.APIServices.response.StockResponse;
import com.sprint.mykitchen.APIServices.response.TagResponse;
import com.sprint.mykitchen.APIServices.response.RecipeDetailResponse;
import com.sprint.mykitchen.RegistroActivity.NewUser;
import com.sprint.mykitchen.inventory.model.NewProduct;
import com.sprint.mykitchen.inventory.model.Quantity;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Services {

    @GET("system/tags")
    Call<TagResponse> getTagsService(@Header("Autorization") String token);

    @GET("system/recipes?")
    Call<RecipesResponse> getAllRecipes(@Header("Autorization") String token,
                                     @Query("limit") String limit,
                                     @Query("offset") String offset);

    @GET("system/recipes/tag?")
    Call<RecipesResponse> getRecipesByTag(@Header("Autorization") String token,
                                          @Query("tag_id") String tag,
                                          @Query("limit") String limit,
                                          @Query("offset") String offset);

    @GET("system/recipes/name?")
    Call<RecipesResponse> getRecipeByName(@Header("Autorization") String token,
                                          @Query("name") String name,
                                          @Query("limit") String limit,
                                          @Query("offset") String offset);

    @GET("system/recipes/ingredients?")
    Call<RecipesResponse> getRecipesByIngredients(@Header("Autorization") String token,
                                                  @Query("ingredients") String ingredients,
                                                  @Query("limit") String limit,
                                                  @Query("offset") String offset);

    @GET("system/recipes/{recipe_id}")
    Call<RecipeDetailResponse> getRecipeDetails(@Header("Autorization") String token,
                                                @Path(value ="recipe_id") String recipe_Id);

    @GET("system/products")
    Call<ProductsResponse> getAllProducts(@Header("Autorization") String token);

    @GET("system/product-categories")
    Call<ProductCategoryResponse> getProductsCategories(@Header("Autorization") String token);

    @GET("system/products-categories")
    Call<ProductCategoryResponse> getProductsCategoriesComplete(@Header("Autorization") String token);

    @GET("account/{account_id}/kitchen/{kitchen_id}/stock")
    Call<StockResponse> getProductStoke(@Header("Autorization") String token,
                                        @Path(value = "account_id") String account_id,
                                        @Path(value = "kitchen_id") String kitchen_id);

    @POST("/login")
    Call<LoginGenericResponse> login(@Header("Autorization") String token, @Body Login user);

    @POST("/system/accounts")
    Call<SingUpGenericResponse> singUp(@Header("Autorization") String token, @Body NewUser user);

    @POST("account/{account_id}/kitchen/{kitchen_id}/stock/{stock_id}")
    Call<GenericResponse> putStock(@Path(value = "account_id") String account_id,
                                   @Path(value = "kitchen_id") String kitchen_id,
                                   @Path(value = "stock_id") String stock_id,
                                   @Body Quantity quantity);

    @POST("account/{account_id}/kitchen/{kitchen_id}/stock")
    Call<GenericResponse> addStock(@Path(value = "account_id") String account_id,
                                   @Path(value = "kitchen_id") String kitchen_id,
                                   @Body NewProduct product);

    @GET("account/recipes?")
    Call<RecipesResponse> getRecipesByStock(@Header("Autorization") String token,
                             @Query("limit") String limit,
                             @Query("offset") String offset);
}
