package com.sprint.mykitchen.OptionsLoginActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.sprint.mykitchen.R
import kotlinx.android.synthetic.main.activity_options_login.*

class OptionsLoginActivity : AppCompatActivity(), OptionLoginContract.View {


    var presenter: OptionLoginContract.Presenter? = OptionsLoginPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_options_login)
        initComponents()
    }
// establece que cuando se dé clic a cada voton se mandará la vista a la funcion OnClick()
    override fun initComponents(){
        loginLocal_btn.setOnClickListener { v -> onClick(v) }
        loginGoogle_btn.setOnClickListener { v -> onClick(v) }
        loginFacebook_btn.setOnClickListener { v -> onClick(v) }
        registrarse_btn.setOnClickListener { v -> onClick(v) }
    }
//checa a qué boton corresponde el click que se dió y hace lo correspondiente
    private fun onClick(v: View){
        when (v){
            loginLocal_btn -> presenter?.goToLocalLogin()
            loginGoogle_btn -> toast("login con google")
            loginFacebook_btn ->toast("login con Facebook")
            registrarse_btn -> presenter?.goToSignUp()
        }
    }
    //Muestra mensaje en pantalla
    private fun toast(mensaje: String){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()
    }

}
