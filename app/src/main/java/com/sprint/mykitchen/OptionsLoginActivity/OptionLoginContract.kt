package com.sprint.mykitchen.OptionsLoginActivity

interface OptionLoginContract {

    interface View{
        fun initComponents()

    }

    interface Presenter{

        fun goToLocalLogin()

        fun goToSignUp()
    }

    interface Interactor{

    }
    interface Router{

        fun toLocalLogin()

        fun toSignUp()
    }
}