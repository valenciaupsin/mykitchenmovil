package com.sprint.mykitchen.OptionsLoginActivity

import android.app.Activity
import android.content.Intent
import com.sprint.mykitchen.LoginActivity.LoginActivity
import com.sprint.mykitchen.RegistroActivity.RegistroActivity

class Router(var activity: Activity?): OptionLoginContract.Router {
//hace el intent a la pantalla de login local para mostrarla
    override fun toLocalLogin(){
        val intent = Intent(activity, LoginActivity::class.java )
        activity?.startActivity(intent)
    }
    //hace el intent a la pantalla de registro para mostrarla
    override fun toSignUp() {
        val intent = Intent(activity, RegistroActivity::class.java )
        activity?.startActivity(intent)
    }
}