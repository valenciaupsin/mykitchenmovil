package com.sprint.mykitchen.OptionsLoginActivity

import android.app.Activity

class OptionsLoginPresenter(view: OptionLoginContract.View) : OptionLoginContract.Presenter {
//variable de router para hacer intent
    private val router : OptionLoginContract.Router = Router(view as? Activity)
//manda llamar a la funcion del router que lleva a la pantalla de login local
    override fun goToLocalLogin(){
        router.toLocalLogin()
    }
    //manda llamar a la funcion del router que lleva a la pantalla de registrarse
    override fun goToSignUp() {
        router.toSignUp()
    }

}