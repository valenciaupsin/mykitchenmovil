package com.sprint.mykitchen.RegistroActivity;

import com.sprint.mykitchen.constants.Errors;

public class RegistroPresenter implements RegistroContract.Presenter {
    private RegistroContract.View view;
    private RegistroContract.Interactor interactor;

    public RegistroPresenter(RegistroActivity view) {
        this.view = view;
        interactor = new RegistroInteractor(this);
    }

    @Override
    public void triggerSingUp(String name, String surname, String email, String password) {
        NewUser newUser = new NewUser(name, surname, email, password);
        interactor.SingUp(newUser);
    }

    @Override
    public void triggerSuccessSingUp(NewUser newUser) {
        view.successSingUp(newUser.getName());
    }

    @Override
    public void triggerError(Errors error) {
        switch (error){
            case DataError: view.Error("El correo ya se encuentra registrado");break;
            case AutorizationError: view.Error("Error de autorización");break;
        }
    }
}
