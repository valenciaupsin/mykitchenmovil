package com.sprint.mykitchen.RegistroActivity;

import com.sprint.mykitchen.constants.Errors;

public interface RegistroContract {
    interface View{
        void successSingUp(String name);

        void Error(String error);
    }

    interface Presenter{
        void triggerSingUp(String name, String surname, String email, String password);

        void triggerSuccessSingUp(NewUser content);

        void triggerError(Errors error);
    }

    interface Interactor{
        void SingUp(NewUser newUser);
    }

    interface Router{

    }
}
