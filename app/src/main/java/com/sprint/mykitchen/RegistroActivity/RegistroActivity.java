package com.sprint.mykitchen.RegistroActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sprint.mykitchen.LoginActivity.LoginActivity;
import com.sprint.mykitchen.R;

public class RegistroActivity extends AppCompatActivity implements RegistroContract.View {

    private EditText nombre_et;
    private EditText apellido_et;
    private EditText correo_et;
    private EditText contraseña_et;
    private EditText confirm_contraseña_et;
    private Button registrarse_btn;
    private Button cancelar_btn;

    private RegistroContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        initComponents();
    }

    private void initComponents() {
        presenter = new RegistroPresenter(this);
        nombre_et = findViewById(R.id.nombre_et);
        apellido_et = findViewById(R.id.apellido_et);
        correo_et = findViewById(R.id.correo_et);
        contraseña_et = findViewById(R.id.contrasena_et);
        confirm_contraseña_et = findViewById(R.id.confirm_contrasena_et);
        registrarse_btn = findViewById(R.id.registrarse_btn);
        cancelar_btn = findViewById(R.id.cancelar_btn);
        registrarse_btn.setOnClickListener(this::onClick);
        cancelar_btn.setOnClickListener(this::onClick);

    }

    private void onClick(View v) {
        if (v == registrarse_btn){
            registerOk();
        }else if (v == cancelar_btn){
            finish();
        }
    }

    private void registerOk() {
        if(validateEmpty(nombre_et) && validateEmpty(apellido_et) && validateEmpty(correo_et) && validateEmpty(contraseña_et) && validateEmpty(confirm_contraseña_et) ){
            if(validatePassword()){
                presenter.triggerSingUp(nombre_et.getText().toString(), apellido_et.getText().toString(), correo_et.getText().toString(), contraseña_et.getText().toString());
            }else toast("Las contraseñas no coinciden");
        }
    }

    @Override
    public void successSingUp(String name) {
        Toast.makeText(this, "Bienvenido a la familia MyKitchen "+ name +", inicia sesión para poder empezar a llenar tu pripio inventario", Toast.LENGTH_LONG).show();
//        Intent intent = new Intent(this, LoginActivity.class);
//        startActivity(intent);
        finish();
    }

    @Override
    public void Error(String error) {
        toast(error);
    }

    private boolean validatePassword() {
        if (confirm_contraseña_et.getText().toString().equals(contraseña_et.getText().toString())){
            return true;
        }else{
            return false;
        }

    }

    private boolean validateEmpty(EditText editText) {
        if (editText.getText().toString().equals("")){
            editText.setError("Campo Vacio");
            return false;
        }
        return true;
    }
    private void toast(String mensaje){
        Toast.makeText(this,mensaje, Toast.LENGTH_SHORT).show();
    }


}
