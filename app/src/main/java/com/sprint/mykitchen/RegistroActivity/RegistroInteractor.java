package com.sprint.mykitchen.RegistroActivity;

import com.sprint.mykitchen.APIServices.APIAdapter;
import com.sprint.mykitchen.APIServices.response.SingUpGenericResponse;
import com.sprint.mykitchen.constants.Errors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroInteractor implements RegistroContract.Interactor {

    private RegistroContract.Presenter presenter;
    private String token = "S<yus%;|ZO'1k/ISa^H+6_,!:&$0Z+kM9)B?;f`=]=p%q!)uJ^x_!F!7!LL&F|B";

    public RegistroInteractor(RegistroPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void SingUp(NewUser newUser) {
        Thread thread = new Thread(() -> {
            Call<SingUpGenericResponse> call = APIAdapter.getApiService(token).singUp(token, newUser);
            call.enqueue(new Callback<SingUpGenericResponse>() {
                @Override
                public void onResponse(Call<SingUpGenericResponse> call, Response<SingUpGenericResponse> response) {
                    if (response.isSuccessful()){
                        switch (response.code()){
                            case 200: presenter.triggerSuccessSingUp(response.body().getContent());break;
                            case 202: presenter.triggerError(Errors.DataError);break;
                            case 401: presenter.triggerError(Errors.AutorizationError);break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<SingUpGenericResponse> call, Throwable t) {

                }
            });
        });
        thread.start();
    }
}
