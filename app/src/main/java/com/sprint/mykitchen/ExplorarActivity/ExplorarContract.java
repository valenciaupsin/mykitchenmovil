package com.sprint.mykitchen.ExplorarActivity;

import android.content.Context;

import com.sprint.mykitchen.APIServices.response.ProductCategoryResponse;
import com.sprint.mykitchen.ExplorarActivity.model.ProductCategory;
import com.sprint.mykitchen.ExplorarActivity.model.Tag;
import com.sprint.mykitchen.constants.Constants;
import com.sprint.mykitchen.ExplorarActivity.model.RecipeShort;
import com.sprint.mykitchen.APIServices.response.RecipesResponse;
import com.sprint.mykitchen.APIServices.response.TagResponse;

import java.util.ArrayList;

public interface ExplorarContract {

    interface View{

        void triggerRequestProductsCategories();

        void triggerRequestRecipesByTag(String tag);

        void triggerRequestRecipesByProducts(String products);


        //void triggerRequestAllProducts();

        //void sendProducts(ArrayList<Product> productsList);

        void sendTags(ArrayList<Tag> tagsList);

        void sendProductsCategories(ArrayList<ProductCategory> content);

        void sendRecipes(ArrayList<RecipeShort> recipesList);

        void triggerGoToRecipeDetails(String recipeId);

        void onRecipesTagError();
        void onConnectionError();
        void onRecipesNameError();
        void onRecipesIngredientsError();


    }

    interface TagsFragment {
        void setTags(ArrayList<Tag> tagsList);
    }

    interface Presenter{

        //PIDE
        void requestTags();
//        void requestAllRecipes();
        void requestRecipesByTag(String tag);
//        void requestRecipesByName(String name);
        void requestRecipesByIngredients(String products);
//        void requestAllProducts();
        void requestRecipesByStock(String TOKEN);
        void requestProductsCategories();

        //RECIBE
        void triggerSetTags(TagResponse tagResponse);
        void triggerSetRecipes(RecipesResponse recipesResponse);
        //void triggerSetAllProducts(ProductsResponse productsResponse);
        void triggerSetError(Constants error);
        void triggerSetProductCategories(ProductCategoryResponse productCategoryResponse);
        //DIRIGE
        void goToSingIn(Context context);
        void goToRecipeDetails(Context context, String recipeId);

    }
    interface Interactor{

        void getTags();
        void getAllRecipes();
        void getRecipesByTag(String tag);
        void getRecipesByName(String name);
        void getRecipesByIngredients(String ingredients);
//        void getAllProducts();
//        void getProductCategories();
        void getProductCategoriesComplete();
        void getRecipesByStock(String TOKEN);

    }

    interface Router{
        void toSingUp(Context context);
        void toSingIn(Context context);
        void toRecipeDetails(Context context, String recipeId);

    }

}
