package com.sprint.mykitchen.ExplorarActivity.tagsFragment;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sprint.mykitchen.ExplorarActivity.model.Tag;
import com.sprint.mykitchen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TagRecyclerAdapter extends RecyclerView.Adapter<TagRecyclerAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Tag> tagsList;

    public TagRecyclerAdapter(Context context, ArrayList tagsList) {
        this.context = context;
        this.tagsList = tagsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tag_layout,null, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tagName.setText(tagsList.get(position).getName());
        Picasso.with(context).load("https://s3.amazonaws.com/images.mykitchen.com.mx/tags/"+tagsList.get(position).getImage())
                .into(holder.tagImage);
    }

    @Override
    public int getItemCount() {
        return tagsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView tagImage;
        private TextView tagName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tagImage = itemView.findViewById(R.id.seccion_iv);
            tagName = itemView.findViewById(R.id.seccion_tv);
        }
    }
}
