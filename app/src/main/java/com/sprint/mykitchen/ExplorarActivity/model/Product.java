package com.sprint.mykitchen.ExplorarActivity.model;

import java.util.ArrayList;

public class Product {
    private String id="";
    private String name="";
    private String image="";
    private String productcategoryId="";
    private String createdAt = "";
    private String updatedAt = "";
    private ArrayList<Subproduct> subproducts;

    private boolean isSelected = false;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getProductCategoryId() {
        return productcategoryId;
    }

    public String getProductcategoryId() {
        return productcategoryId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }


    public ArrayList<Subproduct> getSubproducts() {
        return subproducts;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


}
