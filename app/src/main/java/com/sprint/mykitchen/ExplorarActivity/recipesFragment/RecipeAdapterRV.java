package com.sprint.mykitchen.ExplorarActivity.recipesFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sprint.mykitchen.ExplorarActivity.model.RecipeShort;
import com.sprint.mykitchen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecipeAdapterRV extends RecyclerView.Adapter<RecipeAdapterRV.ViewHolderRecipes> implements View.OnClickListener{
    private Context context;
    ArrayList<RecipeShort> recipeList;
    private View.OnClickListener listener;

    public RecipeAdapterRV(Context context, ArrayList<RecipeShort> recipeList) {
        this.context = context;
        this.recipeList = recipeList;
    }

    @NonNull
    @Override
    public ViewHolderRecipes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_card,null, false);
        view.setOnClickListener(this);
        return new ViewHolderRecipes(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolderRecipes holder, int position) {
        //holder.recipeImage.setImageResource(recipeList.get(position).getImage());
        Picasso.with(context).load("https://s3.amazonaws.com/images.mykitchen.com.mx/recipes/"+recipeList.get(position).getImage())
                .into(holder.recipeImage);
        if (recipeList.get(position).getTotal_ingredients() == 0 && recipeList.get(position).getTotal_owned() == 0){
            holder.ingreLeft.setVisibility(View.GONE);
        }else{
            holder.ingreLeft.setVisibility(View.VISIBLE);
            int ingreLeftTotal = recipeList.get(position).getTotal_ingredients() - recipeList.get(position).getTotal_owned();
            holder.ingreLeft.setText(context.getResources().getString(R.string.ingredientes_faltantes) + ingreLeftTotal);
        }

        holder.nameTV.setText(recipeList.get(position).getName());
        holder.caloriesTV.setText(recipeList.get(position).getCalories() + context.getString(R.string.calories_abreviation ));
        holder.timeTV.setText(recipeList.get(position).getTime() + context.getString(R.string.time_abreviation));
        String dificulty ="-";
        switch (Integer.parseInt(recipeList.get(position).getDifficultyId())){
            case 1: dificulty = "Fácil";break;
            case 2: dificulty = "Intermedio";break;
            case 3: dificulty = "Difícil";break;
            default: dificulty = "-";break;
        }
        holder.difficultTV.setText(dificulty);
    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;

    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolderRecipes extends RecyclerView.ViewHolder {

        ImageView recipeImage;
        TextView nameTV;
        TextView ingreLeft;
        TextView caloriesTV;
        TextView timeTV;
        TextView difficultTV;

        public ViewHolderRecipes(@NonNull View itemView) {
            super(itemView);
            recipeImage = itemView.findViewById(R.id.imageRecipeCardView);
            nameTV = itemView.findViewById(R.id.titleRecipeCardView);
            ingreLeft = itemView.findViewById(R.id.ingreLeft);
            caloriesTV = itemView.findViewById(R.id.caloriesRecipeCardView);
            timeTV = itemView.findViewById(R.id.timeRecipeCardView);
            difficultTV = itemView.findViewById(R.id.dificultRecipeCardView);
        }
    }
}
