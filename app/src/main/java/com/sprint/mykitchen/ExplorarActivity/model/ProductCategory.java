package com.sprint.mykitchen.ExplorarActivity.model;

import java.util.ArrayList;

public class ProductCategory {
    private int id = 0;
    private String name = "";
    private String image = "";
    private ArrayList<Product> product;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public ArrayList<Product> getProduct() {
        return product;
    }
}
