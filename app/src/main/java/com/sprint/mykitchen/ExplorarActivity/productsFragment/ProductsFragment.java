package com.sprint.mykitchen.ExplorarActivity.productsFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.sprint.mykitchen.ExplorarActivity.ExplorarActivity;
import com.sprint.mykitchen.ExplorarActivity.ExplorarContract;
import com.sprint.mykitchen.ExplorarActivity.model.ProductCategory;
import com.sprint.mykitchen.R;
import com.sprint.mykitchen.ExplorarActivity.model.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ProductsFragment extends Fragment {

    private ArrayList<Product> productsList;
    ExpandableList_Adapter_Products expandableListAdapterProducts;
    private View view;
    private ExpandableListView expandableListView;
    private Button search_btn;
    private ProgressBar progressBar;
    private ExplorarContract.View activity;
    private HashMap<ProductCategory, List<Product>> item = new HashMap<>();
    private ChipGroup selected_cg;

    public ProductsFragment() {
        // Required empty public constructor
    }
    public ProductsFragment(ExplorarActivity activity){
        this.activity = activity;
    }

    public ProductsFragment(ExplorarActivity activity, HashMap<ProductCategory, List<Product>> productsList){
        this.activity = activity;
        item= productsList;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_products, container, false);

        initComponents();
        setProductsList();

        return view;
    }

    private void initComponents() {
        search_btn = view.findViewById(R.id.search_btn);
        search_btn.setOnClickListener((v -> getSelectedProducts()));
        progressBar = view.findViewById(R.id.productsProgressBar);
        expandableListView = view.findViewById(R.id.expandableListProducts);
        selected_cg = view.findViewById(R.id.selected_cg);
    }

    private void setProductsList() {
        progressBar.setVisibility(View.GONE);
        expandableListAdapterProducts = new ExpandableList_Adapter_Products(item);
        expandableListView.setAdapter(expandableListAdapterProducts);
        onClick();
    }


    private void onClick(){
        productsList = new ArrayList<>();
        expandableListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {

            Product product = (Product) expandableListAdapterProducts.getChild(groupPosition, childPosition);
            if(!productsList.contains(product)){
                product.setSelected(true);
                productsList.add(product);
            }else {
                product.setSelected(false);
                productsList.remove(product);
            }
            selected_cg.removeAllViews();
            for (Product product1: productsList){
                Chip chip = new Chip(getContext());
                chip.setText(product1.getName());
                selected_cg.addView(chip);
            }

            return true;
        });

    }

    private void getSelectedProducts(){
        String text = "";
        if (productsList.size()>0 ){
            for (Product model : productsList) {
//            if (model.isSelected()) {
                text += model.getId() + ",";
//            }
            }
            text = text.substring(0, text.length() - 1);
            activity.triggerRequestRecipesByProducts(text);
//            Log.d("PRODUCTSFINAL", text);
        }else{
            Toast.makeText(getContext(), "Selecciona al menos un ingrediente para continuar",Toast.LENGTH_SHORT).show();
        }
    }



}
