package com.sprint.mykitchen.ExplorarActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.sprint.mykitchen.ExplorarActivity.model.ProductCategory;
import com.sprint.mykitchen.ExplorarActivity.model.Tag;
import com.sprint.mykitchen.ExplorarActivity.productsFragment.ProductsFragment;
import com.sprint.mykitchen.ExplorarActivity.recipesFragment.RecipesFragment;
import com.sprint.mykitchen.ExplorarActivity.tagsFragment.TagRecyclerAdapter;
import com.sprint.mykitchen.ExplorarActivity.tagsFragment.TagsFragment;
import com.sprint.mykitchen.LoginActivity.LoginActivity;
import com.sprint.mykitchen.constants.Constants;
import com.sprint.mykitchen.ExplorarActivity.model.RecipeShort;

import com.sprint.mykitchen.R;
import com.google.android.material.navigation.NavigationView;
import com.sprint.mykitchen.ExplorarActivity.model.Product;
import com.sprint.mykitchen.inventory.InventoryActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.sprint.mykitchen.constants.Constants.Add;
import static com.sprint.mykitchen.constants.Constants.Replace;

public class ExplorarActivity extends AppCompatActivity implements ExplorarContract.View{

    private String USERMODE = "GUEST";
    private String TOKEN;
    private String KITCHENID;
    private String ACCOUNTID;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private ImageButton imageButton;
    private DrawerLayout drawerLayout;
    private ExplorarContract.Presenter presenter;
    private ExplorarContract.TagsFragment tagsFragment;
    FragmentManager fragmentManager = getSupportFragmentManager();
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    private BottomNavigationView mainBottomNavigation;
    private RecyclerView mainRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navegationview_layout);

        presenter = new ExplorarPresenter(this);
        tagsFragment = new TagsFragment(this);
        initComponents();
        loadFragment((Fragment) tagsFragment, Add);
        triggerRequestGetTags();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        USERMODE = sharedPref.getString("USERMODE", "GUEST");
        TOKEN = sharedPref.getString("TOKEN", "");
        KITCHENID = sharedPref.getString("KITCHENID", "");
        ACCOUNTID = sharedPref.getString("ACCOUNTID", "");
/*        if (USERMODE.equals("GUEST")){
            inventoryChip.setVisibility(View.INVISIBLE);
        }*/

    }

    //INICIALIZA COMPONENTES
    public void initComponents(){
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        navigationView = findViewById(R.id.nav_view);
        if (navigationView != null) { setupDrawerContent(navigationView); }
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout= findViewById(R.id.drawer_layout);

        mainBottomNavigation = findViewById(R.id.main_bottom_navigation);
        mainBottomNavigation.setOnNavigationItemSelectedListener(this::onBottomNavegationClick);

        mainRecyclerView = findViewById(R.id.main_recyclerView);
        mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private boolean onBottomNavegationClick(MenuItem item){
        switch (item.getItemId()){
            case R.id.tag_btn:
                tagsFragment = new TagsFragment(this);
                loadFragment((Fragment)tagsFragment, Replace);
                triggerRequestGetTags();
                return true;
            case R.id.ingredients_btn:
                triggerRequestProductsCategories();
                return true;
            case R.id.inventory_btn:
                presenter.requestRecipesByStock(TOKEN);
                return true;
            default: return false;
        }

    }

    //CONTROLA CLICKS
    public void onClick(View view) {
        /*if (view == imageButton){
            drawerLayout.openDrawer(GravityCompat.START);
        }else if (view == tagsChip){
            tagsChip.setChecked(true);
            tagsFragment = new TagsFragment(this);
            loadFragment((Fragment)tagsFragment, Replace);
            triggerRequestGetTags();
        }else if (view == ingredientsChip){
            ingredientsChip.setChecked(true);
            triggerRequestProductsCategories();
        }else if (view == inventoryChip){
            inventoryChip.setChecked(true);
            presenter.requestRecipesByStock(TOKEN);
        }*/
    }

    //INICIA SOLICITUD DE TAGS
    private void triggerRequestGetTags(){
        presenter.requestTags();
    }

    //MANDA ETIQUETAS A FRAGMENTO
    @Override
    public void sendTags(ArrayList<Tag> tagsList) {
//        tagsFragment.setTags(tagsList);
        TagRecyclerAdapter adapter = new TagRecyclerAdapter(this, tagsList);
        mainRecyclerView.setAdapter(adapter);
    }

    //COMIENZA SOLICITUD DE LISTA DE RECETAS POR ETIQUETA
    public void triggerRequestRecipesByTag(String tag) {
        presenter.requestRecipesByTag(tag);
    }

    @Override
    public void triggerRequestRecipesByProducts(String products) {
        presenter.requestRecipesByIngredients(products);
    }

    //COMIENZA SOLICITUD DE LISTA DE TODAS LAS RECETAS
    /*public void triggerRequestAllRecipes(){
        presenter.requestAllRecipes();
    }*/

    //MANDA LISTA DE RECETAS A FRAGMENTO
    public void sendRecipes(ArrayList<RecipeShort> recipesList) {
//        chipGroup.setVisibility(View.GONE);
        loadFragment(new RecipesFragment(this, recipesList), Add);
    }

    @Override
    public void triggerRequestProductsCategories() {
        presenter.requestProductsCategories();
    }

    @Override
    public void sendProductsCategories(ArrayList<ProductCategory> content) {
        HashMap<ProductCategory, List<Product>> item = new HashMap<>();

        for (int x=0; x<content.size(); x++){
            ArrayList<Product> products = new ArrayList<>(content.get(x).getProduct());
            item.put(content.get(x), products);
        }

        loadFragment(new ProductsFragment(this, item),Replace);
    }

    //CARGA FRAGMENTOS
    void loadFragment(Fragment newFragment, Constants action){
        /*Thread thread = new Thread(() -> {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (action){
                case Add:
                    fragmentTransaction.add(R.id.fragment, newFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    break;
                case Replace:
                    fragmentTransaction.replace(R.id.fragment, newFragment);
                    fragmentTransaction.commit();
                    break;
                case Remove:
                    fragmentTransaction.remove(newFragment);
                    fragmentTransaction.commit();
                    break;
            }

        });
        thread.start();*/

    }

    public void triggerGoToRecipeDetails(String recipeId){
        presenter.goToRecipeDetails(this, recipeId);
    }

    //CONTROLA CLICKS DEL MENU LATERAL
    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    switch (menuItem.getItemId()){
                        case R.id.nav_recetas:
                            drawerLayout.closeDrawer(GravityCompat.START);
//                            Fragment fragment = new TagsFragment();
                            //loadFragment(fragment);
                            break;
                        case R.id.nav_inventario:
                            drawerLayout.closeDrawer(GravityCompat.START);
                            if (USERMODE.equals("GUEST")){
                                Intent intent = new Intent(this, LoginActivity.class);
                                startActivityForResult(intent, 1);
                            }else{
                                Intent intent = new Intent(this, InventoryActivity.class);
                                intent.putExtra("TOKEN", TOKEN);
                                intent.putExtra("KITCHENID", KITCHENID);
                                intent.putExtra("ACCOUNTID", ACCOUNTID);
                                startActivityForResult(intent, 2);
                            }
                            break;
                        case  R.id.nav_cerrarSesion: {
                            drawerLayout.closeDrawer(GravityCompat.START);
                            if (USERMODE.equals("CLIENT")) {
                                logOut();
                                toast("Sesión cerrada");
                            }
                            else
                                toast("No hay una sesión activa apara cerrar");
                            break;
                        }
                        case R.id.nav_salir:
                            finish();
                            break;
                    }
                    return true;
                }
        );
    }

    private void logOut(){
        USERMODE = "GUEST";
        TOKEN = "";
        KITCHENID = "";
        ACCOUNTID = "";
        editor.putString("USERMODE", "GUEST");
        editor.putString("TOKEN", "");
        editor.putString("KITCHENID", "");
        editor.putString("ACCOUNTID", "");
        editor.apply();
//        inventoryChip.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK){
            try {
                if (data.hasExtra("USERMODE") && data.hasExtra("TOKEN") && data.hasExtra("KITCHENID") && data.hasExtra("ACCOUNTID")) {
                    USERMODE = data.getExtras().getString("USERMODE");
                    TOKEN = data.getExtras().getString("TOKEN");
                    KITCHENID = data.getExtras().getString("KITCHENID");
                    ACCOUNTID = data.getExtras().getString("ACCOUNTID");

                    editor.putString("USERMODE", USERMODE);
                    editor.putString("TOKEN", TOKEN);
                    editor.putString("KITCHENID", KITCHENID);
                    editor.putString("ACCOUNTID", ACCOUNTID);
                    editor.apply();

//                    inventoryChip.setVisibility(View.VISIBLE);
                }
            }catch (Throwable e){
                logOut();
            }

        }
    }

    //CREA EL MENU DE OPCIONES DE LA TOOLBAR
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
            getMenuInflater().inflate(R.menu.explorar_toolbar_menu, menu);
            MenuItem searchItem = menu.findItem(R.id.search);
            searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    toast(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return true;
                }
            });
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }*/

    //CONTROLA CLICKS DE MENU DE TOOLBAR
   /* @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.search:
                //triggerRequestAllRecipes();
                break;
                //return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    //CONTROLA LA ACCIONA AL PRESIONAR ATRÁS
    @Override
    public void onBackPressed() {
       /* FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragmentView = fragmentManager.findFragmentById(R.id.fragment);
        if(fragmentView instanceof RecipesFragment) {
            fragmentTransaction.remove(fragmentView);
            fragmentTransaction.commit();
//            chipGroup.setVisibility(View.VISIBLE);
        }*/
        super.onBackPressed();
    }

    @Override
    public void onRecipesTagError() {
        toast(getString(R.string.recetas_etiqueta_error));
    }

    @Override
    public void onRecipesNameError() {
        toast(getString(R.string.recetas_nombre_error));
    }

    @Override
    public void onRecipesIngredientsError() {
        toast(getString(R.string.recetas_ingrediente_error));
    }

    @Override
    public void onConnectionError() {
        toast(getString(R.string.error_conexion));
    }

    //HACE UN TOAST
    private void toast(String mensaje){ Toast.makeText(ExplorarActivity.this,mensaje,Toast.LENGTH_SHORT).show(); }
}
