package com.sprint.mykitchen.ExplorarActivity.tagsFragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sprint.mykitchen.ExplorarActivity.model.Tag;
import com.sprint.mykitchen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TagAdapter extends BaseAdapter {

    Context context;
    ArrayList<Tag> tags;
    LayoutInflater inflater;

    public TagAdapter(Context context, ArrayList<Tag> tags) {
        this.context = context;
        this.tags = tags;
    }


    @Override
    public int getCount() {
        return tags.size();
    }

    @Override
    public Object getItem(int position) {
        return tags.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView ==null){
            convertView = inflater.inflate(R.layout.tag_layout, null);

        }
            ImageView image = convertView.findViewById(R.id.seccion_iv);
            TextView title = convertView.findViewById(R.id.seccion_tv);

            //image.setImageResource(R.drawable.mykitchen_logo);
            Picasso.with(context).load("https://s3.amazonaws.com/images.mykitchen.com.mx/tags/"+tags.get(position).getImage())
                    .into(image);
            title.setText(tags.get(position).getName());


        return convertView;
    }


}
