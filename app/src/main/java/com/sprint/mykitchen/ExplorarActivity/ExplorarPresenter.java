package com.sprint.mykitchen.ExplorarActivity;

import android.content.Context;

import com.sprint.mykitchen.APIServices.response.ProductCategoryResponse;
import com.sprint.mykitchen.constants.Constants;
import com.sprint.mykitchen.APIServices.response.RecipesResponse;
import com.sprint.mykitchen.APIServices.response.TagResponse;

public class ExplorarPresenter implements ExplorarContract.Presenter {

    private ExplorarContract.Interactor interactor;
    private ExplorarContract.View view;
    private ExplorarContract.Router router;


    ExplorarPresenter(ExplorarActivity view) {
        this.view = view;
        interactor= new ExplorarInteractor(this);
        router = new ExplorarRouter(this);
    }

    //Pide la lista de tags
    @Override
    public void requestTags() {
        interactor.getTags();
    }

    /*@Override
    public void requestAllRecipes() {
        interactor.getAllRecipes();
    }*/

    //Pide al router mandar la tag seleccionada al servicio
    public void requestRecipesByTag(String tag){
        //interactor = new ExplorarInteractor(this);
        interactor.getRecipesByTag(tag);
    }

    /*@Override
    public void requestRecipesByName(String name) { interactor.getRecipesByName(name); }*/

    @Override
    public void requestRecipesByIngredients(String products) {
        interactor.getRecipesByIngredients(products);
    }

    @Override
    public void requestProductsCategories() {
        //interactor.getProductCategories();
        interactor.getProductCategoriesComplete();
    }

    /*@Override
    public void requestAllProducts() {
        interactor.getAllProducts();
    }*/


    @Override
    public void triggerSetProductCategories(ProductCategoryResponse productCategoryResponse) {
        view.sendProductsCategories(productCategoryResponse.getContent());
    }

    /*@Override
    public void triggerSetAllProducts(ProductsResponse productsResponse) {
        if(productsResponse.getContent().size() != 0){
            view.sendProducts(productsResponse.getContent());
        }
    }*/

    //pone las tags de la respuesta en un modelo SeccionModel
    @Override
    public void triggerSetTags(TagResponse response){
        if (response.getContent() != null) {
            view.sendTags(response.getContent());}
        else {
            triggerSetError(Constants.NoTags);
        }
    }

    @Override
    public void triggerSetRecipes(RecipesResponse response) {
        if (response.getContent().size() != 0) {
            view.sendRecipes(response.getContent());}
        else {
            triggerSetError(Constants.NoRecipesTag);
        }
    }

    @Override
    public void triggerSetError(Constants error){
        switch (error){
            case NoRecipesTag:
                view.onRecipesTagError();
                break;
            case NoRecipesName:
                view.onRecipesNameError();
                break;
            case NoRecipesIngredient:
                view.onRecipesIngredientsError();
                break;
            case Connection_Fail:
                view.onConnectionError();
                break;
        }
    }

    //Pide al router que inicie activity de inicio de secion
    @Override
    public void goToSingIn(Context context) {
        //router = new ExplorarRouter(this);
        router.toSingIn(context);
    }

    @Override
    public void goToRecipeDetails(Context context, String recipeId) {
        //router = new ExplorarRouter(this);
        router.toRecipeDetails(context, recipeId);
    }

    @Override
    public void requestRecipesByStock(String TOKEN) {
        interactor.getRecipesByStock(TOKEN);
    }
}
