package com.sprint.mykitchen.ExplorarActivity.model;

import com.sprint.mykitchen.R;

public class Tag {
    private int id;
    private String image;
    private String name;

    public Tag() {
    }

    public Tag(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Tag(int id, String image, String name) {
        this.id = id;
        this.image = image;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getImage() {
        return image;
    }


    public String getName() {
        return name;
    }

}
