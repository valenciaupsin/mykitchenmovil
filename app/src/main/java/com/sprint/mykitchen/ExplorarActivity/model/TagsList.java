package com.sprint.mykitchen.ExplorarActivity.model;

import java.util.ArrayList;

public class TagsList {
    private String id;
    private Tag tag;

    public TagsList() {
    }

    public TagsList(String id, Tag tag) {
        this.id = id;
        this.tag = tag;
    }

    public String getId() {
        return id;
    }

    public Tag getTag() {
        return tag;
    }
}
