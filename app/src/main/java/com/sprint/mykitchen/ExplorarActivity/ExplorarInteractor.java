package com.sprint.mykitchen.ExplorarActivity;

import android.util.Log;

import com.sprint.mykitchen.APIServices.APIAdapter;
import com.sprint.mykitchen.APIServices.response.ProductCategoryResponse;
import com.sprint.mykitchen.APIServices.response.ProductsResponse;
import com.sprint.mykitchen.constants.Constants;
import com.sprint.mykitchen.APIServices.response.RecipesResponse;
import com.sprint.mykitchen.APIServices.response.TagResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExplorarInteractor implements ExplorarContract.Interactor{

    ExplorarContract.Presenter presenter;

    public ExplorarInteractor(ExplorarPresenter presenter) {
        this.presenter = presenter;
    }

    private String token = "S<yus%;|ZO'1k/ISa^H+6_,!:&$0Z+kM9)B?;f`=]=p%q!)uJ^x_!F!7!LL&F|B";
    private String limit = "50";
    private String offset = "0";


    //Recibe las tags
    @Override
    public void getTags(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<TagResponse> call = APIAdapter.getApiService(token).getTagsService(token);
                call.enqueue(new Callback<TagResponse>() {
                    @Override
                    public void onResponse(Call<TagResponse> call, Response<TagResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetTags(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<TagResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();
    }

    //Recibe todas las recetas
    @Override
    public void getAllRecipes() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<RecipesResponse> call = APIAdapter.getApiService(token).getAllRecipes(token, limit, offset);
                call.enqueue(new Callback<RecipesResponse>() {
                    @Override
                    public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetRecipes(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RecipesResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();


    }

    //Recibe recetas por tag
    @Override
    public void getRecipesByTag(String tag) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<RecipesResponse> call = APIAdapter.getApiService(token).getRecipesByTag(token, tag, limit,offset);
                call.enqueue(new Callback<RecipesResponse>() {
                    @Override
                    public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetRecipes(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RecipesResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();

    }

    //Recibe recetas por nombre
    @Override
    public void getRecipesByName(String name) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<RecipesResponse> call = APIAdapter.getApiService(token).getRecipeByName(token,name, limit, offset);
                call.enqueue(new Callback<RecipesResponse>() {
                    @Override
                    public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetRecipes(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RecipesResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();
    }

    //Recibe recetas por ingredientes
    @Override
    public void getRecipesByIngredients(String ingredients){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<RecipesResponse> call = APIAdapter.getApiService(token).getRecipesByIngredients(token,ingredients,limit, offset);
                call.enqueue(new Callback<RecipesResponse>() {
                    @Override
                    public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetRecipes(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RecipesResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();
    }

    /*@Override
    public void getAllProducts() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<ProductsResponse> call = APIAdapter.getApiService().getAllProducts(token);
                call.enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetAllProducts(response.body());break;
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.NoProducts);
                    }
                });
            }
        });
        thread.start();
    }*/

    /*@Override
    public void getProductCategories() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<ProductCategoryResponse> call = APIAdapter.getApiService().getProductsCategories(token);
                call.enqueue(new Callback<ProductCategoryResponse>() {
                    @Override
                    public void onResponse(Call<ProductCategoryResponse> call, Response<ProductCategoryResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetProductCategories(response.body());break;
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductCategoryResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.NoProducts);
                    }
                });
            }
        });
        thread.start();
    }*/

    @Override
    public void getProductCategoriesComplete() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<ProductCategoryResponse> call = APIAdapter.getApiService(token).getProductsCategoriesComplete(token);
                call.enqueue(new Callback<ProductCategoryResponse>() {
                    @Override
                    public void onResponse(Call<ProductCategoryResponse> call, Response<ProductCategoryResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetProductCategories(response.body());break;
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductCategoryResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.NoProducts);
                    }
                });
            }
        });
        thread.start();
    }

    @Override
    public void getRecipesByStock(String TOKEN) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<RecipesResponse> call = APIAdapter.getApiService2(TOKEN).getRecipesByStock(TOKEN, limit,offset);
                call.enqueue(new Callback<RecipesResponse>() {
                    @Override
                    public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetRecipes(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RecipesResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();

    }
}
