package com.sprint.mykitchen.ExplorarActivity.productsFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sprint.mykitchen.ExplorarActivity.model.ProductCategory;
import com.sprint.mykitchen.R;
import com.sprint.mykitchen.ExplorarActivity.model.Product;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class ExpandableList_Adapter_Products extends BaseExpandableListAdapter {
    private HashMap<ProductCategory, List<Product>> mStringListMap;
    private ProductCategory[] mListHeaderGroup;

    public ExpandableList_Adapter_Products(HashMap<ProductCategory, List<Product>> mStringListMap) {
        this.mStringListMap = mStringListMap;
        this.mListHeaderGroup = mStringListMap.keySet().toArray(new ProductCategory[0]);
    }

    @Override
    public int getGroupCount() {
        return mListHeaderGroup.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return Objects.requireNonNull(mStringListMap.get(mListHeaderGroup[groupPosition])).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mListHeaderGroup[groupPosition];
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return Objects.requireNonNull(mStringListMap.get(mListHeaderGroup[groupPosition])).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition*childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView ==null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.products_category_item, parent, false);
            holder = new ViewHolder();
            holder.groupName = convertView.findViewById(R.id.productCategory_tv);
            holder.imageCategory = convertView.findViewById(R.id.imageCategory_iv);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        ProductCategory productCategory = new ProductCategory();
        productCategory = (ProductCategory) getGroup(groupPosition);
        holder.groupName.setText(productCategory.getName());
        Picasso.with(parent.getContext()).load("https://s3.amazonaws.com/images.mykitchen.com.mx/categories/"+productCategory.getImage()).into(holder.imageCategory);
        return convertView;


    }

    private boolean inventaryMode = false;
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingred_item, parent, false);
            holder = new ViewHolder();
            holder.imageView = convertView.findViewById(R.id.imageIngre_iv);
            holder.name = convertView.findViewById(R.id.nombreIngre_tv);
            holder.quantity_btns = convertView.findViewById(R.id.quantity_btns);
            holder.mas = convertView.findViewById(R.id.menosIngre_btn);
            holder.quantity_tv = convertView.findViewById(R.id.cantidadIngre_tv);
            holder.menos = convertView.findViewById(R.id.masIngre_btn);
            holder.unit = convertView.findViewById(R.id.unidadIngre_tv);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        Product product = new Product();
        if (getChild(groupPosition,childPosition) != null)
             product= (Product)getChild(groupPosition,childPosition);

            holder.quantity_btns.setVisibility(View.GONE);


        Picasso.with(parent.getContext()).load("https://s3.amazonaws.com/images.mykitchen.com.mx/products/"+product.getImage()).into(holder.imageView);
        holder.name.setText(product.getName());
        return convertView;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolder{
        private TextView groupName;
        private ImageView imageView;
        private ImageView imageCategory;
        private TextView name;
        private LinearLayout quantity_btns;
        private ImageButton mas;
        private TextView quantity_tv;
        private ImageButton menos;
        private TextView unit;
    }
}
