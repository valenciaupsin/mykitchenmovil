package com.sprint.mykitchen.ExplorarActivity.recipesFragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.sprint.mykitchen.ExplorarActivity.ExplorarActivity;
import com.sprint.mykitchen.ExplorarActivity.ExplorarContract;
import com.sprint.mykitchen.ExplorarActivity.model.RecipeShort;
import com.sprint.mykitchen.R;

import java.util.ArrayList;

public class RecipesFragment extends Fragment{

    private View view;
    private RecyclerView recyclerView;
    private RecipeAdapterRV adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<RecipeShort> recipesList;
    private ProgressBar progressBar;
    private ChipGroup tagsChipGroup;
    private ExplorarContract.View activity;


    public RecipesFragment() {
        // Required empty public constructor
    }

    public RecipesFragment(ExplorarActivity activity, ArrayList<RecipeShort> recipesList) {
        this.activity = activity;
        this.recipesList = recipesList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view  = inflater.inflate(R.layout.fragment_recipes, container, false);
        initComponents();
        setRecipesList();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    private void initComponents(){
        progressBar = view.findViewById(R.id.recipesProgressBar);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView = view.findViewById(R.id.recipesReciclerView);
        tagsChipGroup = view.findViewById(R.id.tagsChipGroup);
    }

    private void setRecipesList() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RecipeAdapterRV(getContext(),recipesList);
        adapter.setOnClickListener(v -> onClick(recipesList.get(recyclerView.getChildAdapterPosition(v)).getId()));
        recyclerView.setAdapter(adapter);
    }

    private void onClick(String recipeId){
        activity.triggerGoToRecipeDetails(recipeId);
    }

    void newTagChip(String tagText){
        Chip tag = new Chip(getContext());
        tag.setText(tagText);
        tag.setClickable(false);
        tagsChipGroup.addView(tag);
    }

    private void toast(String mensaje){
        Toast.makeText(getContext(),mensaje,Toast.LENGTH_SHORT).show();
    }

}
