package com.sprint.mykitchen.ExplorarActivity;

import android.content.Context;
import android.content.Intent;

import com.sprint.mykitchen.OptionsLoginActivity.OptionsLoginActivity;
import com.sprint.mykitchen.RecipeActivity.RecipeActivity;
import com.sprint.mykitchen.RecipeActivity.model.RecipeDetail;
import com.sprint.mykitchen.RegistroActivity.RegistroActivity;

public class ExplorarRouter implements ExplorarContract.Router {

    private ExplorarContract.Presenter presenter;
    public ExplorarRouter(ExplorarPresenter presenter) {
        this.presenter = presenter;
    }

    public void toSingUp(Context context) {
        Intent intent = new Intent(context, RegistroActivity.class);
        context.startActivity(intent);

    }

    @Override
    public void toSingIn(Context context) {
        Intent intent = new Intent(context, OptionsLoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void toRecipeDetails(Context context, String recipeId) {
        Intent intent = new Intent(context, RecipeActivity.class);
        intent.putExtra("RECIPE_ID", recipeId);
        context.startActivity(intent);
    }
}
