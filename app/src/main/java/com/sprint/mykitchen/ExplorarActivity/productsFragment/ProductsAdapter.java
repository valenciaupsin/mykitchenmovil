package com.sprint.mykitchen.ExplorarActivity.productsFragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sprint.mykitchen.R;
import com.sprint.mykitchen.ExplorarActivity.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolderProducts> implements View.OnClickListener {

    private ArrayList<Product> productList;
    private Context context;
    private View.OnClickListener listener;

    public ProductsAdapter(ArrayList<Product> productList, Context context) {
        this.productList = productList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderProducts onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, null, false);
        view.setOnClickListener(this);
        return new ViewHolderProducts(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsAdapter.ViewHolderProducts holder, int position) {
        Picasso.with(context).load("https://s3.amazonaws.com/images.mykitchen.com.mx/products/"+productList.get(position).getImage()).into(holder.imageProduct);
        holder.nameProduct.setText(productList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    public class ViewHolderProducts extends RecyclerView.ViewHolder{
        ImageView imageProduct;
        TextView nameProduct;

        public ViewHolderProducts(@NonNull View itemView) {
            super(itemView);
            imageProduct = itemView.findViewById(R.id.imageProduct_iv);
            nameProduct = itemView.findViewById(R.id.nameProduct_tv);
        }
    }


}
