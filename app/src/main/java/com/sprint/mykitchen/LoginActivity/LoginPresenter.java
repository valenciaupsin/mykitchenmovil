package com.sprint.mykitchen.LoginActivity;

import android.util.Log;

import com.sprint.mykitchen.APIServices.response.LoginGenericResponse;
import com.sprint.mykitchen.constants.Errors;
import com.sprint.mykitchen.inventory.model.Account;

import retrofit2.Response;

public class LoginPresenter implements LoginContract.Presenter {
    private LoginContract.View view;
    private LoginContract.Interactor interactor;

    public LoginPresenter(LoginActivity view) {
        this.view = view;
        this.interactor = new LoginInteractor(this);
    }

    @Override
    public void triggerLogin(String email, String password) {
        interactor.login(email, password);
    }

    @Override
    public void triggerLoginResponse(Response<LoginGenericResponse> response) {
        String token = response.body().getContent().getToken();
        String kitchenId = response.body().getContent().getKitchenId();
        String accountId = response.body().getContent().getAccount().getId();
        view.loginSuccess(token, kitchenId, accountId);
    }

    @Override
    public void triggerError(Errors error) {
        switch (error){
            case DataError:view.onError("Verifica correo o contraseña");break;
            case AutorizationError:view.onError("Error de autorización");break;
        }
    }
}
