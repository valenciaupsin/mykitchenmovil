package com.sprint.mykitchen.LoginActivity;

import com.sprint.mykitchen.APIServices.response.LoginGenericResponse;
import com.sprint.mykitchen.constants.Errors;

import retrofit2.Response;

public interface LoginContract {

    interface View{

        void onError(String error);

        void loginSuccess(String token, String kitchenId, String accoutId);
    }
    interface Presenter{
        void triggerLogin(String email, String password);

        void triggerLoginResponse(Response<LoginGenericResponse> response);

        void triggerError(Errors dataError);
    }
    interface Interactor{
        void login(String email, String password);
    }

}
