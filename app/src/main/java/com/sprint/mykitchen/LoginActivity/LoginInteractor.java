package com.sprint.mykitchen.LoginActivity;

import android.util.Log;

import com.sprint.mykitchen.APIServices.APIAdapter;
import com.sprint.mykitchen.APIServices.response.Login;
import com.sprint.mykitchen.APIServices.response.LoginGenericResponse;
import com.sprint.mykitchen.constants.Errors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginInteractor implements LoginContract.Interactor {

    private LoginContract.Presenter presenter;
    private String token = "S<yus%;|ZO'1k/ISa^H+6_,!:&$0Z+kM9)B?;f`=]=p%q!)uJ^x_!F!7!LL&F|B";

    public LoginInteractor(LoginPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void login(String email, String password) {
        Login user = new Login(email, password);
        Thread thread = new Thread(() -> {
            Call<LoginGenericResponse> call = APIAdapter.getApiService(token).login(token, user);
            call.enqueue(new Callback<LoginGenericResponse>() {
                @Override
                public void onResponse(Call<LoginGenericResponse> call, Response<LoginGenericResponse> response) {
                    switch (response.code()){
                        case 200: presenter.triggerLoginResponse(response);break;
                        case 202: presenter.triggerError(Errors.DataError);break;
                        case 401: presenter.triggerError(Errors.AutorizationError);break;
                    }

                }

                @Override
                public void onFailure(Call<LoginGenericResponse> call, Throwable t) {
                    Log.d("ERRORRESPONSE", t.toString());
                }
            });
        });
        thread.start();
    }
}
