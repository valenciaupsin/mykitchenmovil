package com.sprint.mykitchen.LoginActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sprint.mykitchen.ExplorarActivity.ExplorarActivity;
import com.sprint.mykitchen.R;
import com.sprint.mykitchen.RegistroActivity.RegistroActivity;

public class LoginActivity extends AppCompatActivity implements LoginContract.View{

    private LoginContract.Presenter presenter;
    private EditText correo_et;
    private EditText contrasena_et;
    private Button ingresar_btn;
//    private Button olvideContrasena_btn;
    private Button registro_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        presenter = new LoginPresenter(this);
        initComponents();
    }

    private void initComponents() {
        correo_et = findViewById(R.id.correo_et);
        contrasena_et = findViewById(R.id.contrasena_et);
        ingresar_btn = findViewById(R.id.ingresar_btn);
//        olvideContrasena_btn = findViewById(R.id.olvideContrasena_btn);
        ingresar_btn.setOnClickListener(this::onClick);
//        olvideContrasena_btn.setOnClickListener(this::onClick);
        registro_btn = findViewById(R.id.registro_btn);
        registro_btn.setOnClickListener(this::onClick);
    }

    private void onClick(View v) {
        if (v == ingresar_btn){
            login();
//        }else if (v == olvideContrasena_btn){
//         toast("Olvide contraeña");
        }else if (v == registro_btn){
            Intent intent = new Intent(this, RegistroActivity.class);
            startActivity(intent);
        }
    }

    private void login() {
        if (validateEmpty(correo_et) && validateEmpty(contrasena_et)){
            presenter.triggerLogin(correo_et.getText().toString(), contrasena_et.getText().toString());
        }
    }

    @Override
    public void loginSuccess(String token, String kitchenId, String accountId) {
        Intent intent = new Intent(LoginActivity.this, ExplorarActivity.class);
        intent.putExtra("USERMODE", "CLIENT");
        intent.putExtra("TOKEN", token);
        intent.putExtra("KITCHENID", kitchenId);
        intent.putExtra("ACCOUNTID", accountId);
        setResult(RESULT_OK, intent);
        finish();
    }

    private boolean validateEmpty(EditText editText) {
        if (editText.getText().toString().equals("")){
            editText.setError("Campo Vacio");
            return false;
        }
        return true;
    }

    private void toast(String mensaje){
        Toast.makeText(this,mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String error) {
        toast(error);
    }
}
