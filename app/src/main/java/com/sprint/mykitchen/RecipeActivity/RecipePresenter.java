package com.sprint.mykitchen.RecipeActivity;

import com.sprint.mykitchen.APIServices.response.RecipeDetailResponse;

import retrofit2.Response;

public class RecipePresenter implements RecipeContract.Presenter {

    RecipeContract.View view;
    RecipeContract.Interactor interactor;

    public RecipePresenter(RecipeActivity view) {
        this.view = view;
        this.interactor = new RecipeInteractor(this);
    }

    @Override
    public void requestRecipeDetails(String recipeId) {
        interactor.getRecipeDetail(recipeId);
    }

    @Override
    public void triggerSetRecipeDetails(Response<RecipeDetailResponse> response) {
        view.setRecipesRetails(response.body().getContent());
    }
}
