package com.sprint.mykitchen.RecipeActivity.model;


import com.sprint.mykitchen.ExplorarActivity.model.Tag;
import com.sprint.mykitchen.ExplorarActivity.model.TagsList;


import java.util.ArrayList;

public class RecipeDetail {
    private int id;
    private String name;
    private String createdAt;
    private String updatedAt;
    private String difficultyId;
    private String time;
    private String calories;
    private String image;
    private String video;
    private ArrayList<TagsList> tags;
    private ArrayList<group> group;
    private ArrayList<Procedure> procedure;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getDifficultyId() {
        return difficultyId;
    }

    public String getTime() {
        return time;
    }

    public String getCalories() {
        return calories;
    }

    public String getImage() {
        return image;
    }

    public String getVideo() {
        return video;
    }

    public ArrayList<TagsList> getTags() {
        return tags;
    }

    public ArrayList<group> getGroup() {
        return group;
    }

    public ArrayList<Procedure> getProcedure() {
        return procedure;
    }
}
