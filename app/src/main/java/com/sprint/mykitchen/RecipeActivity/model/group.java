package com.sprint.mykitchen.RecipeActivity.model;

import java.util.ArrayList;

public class group {
    private int id;
    private String groupType;
    private ArrayList<Ingredient> ingredients;

    public group() {
    }

    public group(int id, String groupType, ArrayList<Ingredient> ingredients) {
        this.id = id;
        this.groupType = groupType;
        this.ingredients = ingredients;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
