package com.sprint.mykitchen.RecipeActivity;



import android.util.Log;

import com.sprint.mykitchen.APIServices.APIAdapter;
import com.sprint.mykitchen.APIServices.response.RecipeDetailResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeInteractor implements RecipeContract.Interactor {

    RecipeContract.Presenter presenter;
    public RecipeInteractor(RecipePresenter presenter) {
        this.presenter = presenter;
    }

    private String token = "S<yus%;|ZO'1k/ISa^H+6_,!:&$0Z+kM9)B?;f`=]=p%q!)uJ^x_!F!7!LL&F|B";
    private String limit = "10";
    private String offset = "0";

    @Override
    public void getRecipeDetail(String recipeId) {
        Call<RecipeDetailResponse> call = APIAdapter.getApiService(token).getRecipeDetails(token, recipeId);
        call.enqueue(new Callback<RecipeDetailResponse>() {
            @Override
            public void onResponse(Call<RecipeDetailResponse> call, Response<RecipeDetailResponse> response) {
                switch (response.code()){
                    case 200: presenter.triggerSetRecipeDetails(response);break;
                }
            }

            @Override
            public void onFailure(Call<RecipeDetailResponse> call, Throwable t) {
                Log.d("RESPONSE ERROR", "Error 201");
            }
        });

    }
}
