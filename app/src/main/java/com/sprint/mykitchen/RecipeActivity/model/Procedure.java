package com.sprint.mykitchen.RecipeActivity.model;

public class Procedure {
    private String id;
    private int stepNumber;
    private String description;
    private String ingredientId;

    public Procedure() {
    }

    public Procedure(String id, int stepNumber, String description, String ingredientId) {
        this.id = id;
        this.stepNumber = stepNumber;
        this.description = description;
        this.ingredientId = ingredientId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(String ingredientId) {
        this.ingredientId = ingredientId;
    }
}
