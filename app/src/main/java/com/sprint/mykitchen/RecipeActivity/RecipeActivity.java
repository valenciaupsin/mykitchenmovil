package com.sprint.mykitchen.RecipeActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.airbnb.paris.Paris;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
//import com.sprint.mykitchen.ExplorarActivity.model.Recipe;
import com.sprint.mykitchen.ExplorarActivity.model.TagsList;
import com.sprint.mykitchen.MainActivity;
import com.sprint.mykitchen.R;
import com.sprint.mykitchen.RecipeActivity.model.Ingredient;
import com.sprint.mykitchen.RecipeActivity.model.Procedure;
import com.sprint.mykitchen.RecipeActivity.model.RecipeDetail;
import com.sprint.mykitchen.constants.Units;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kotlin.jvm.internal.PropertyReference0Impl;

public class RecipeActivity extends AppCompatActivity implements RecipeContract.View {

    private RecipeContract.Presenter presenter;
    private ImageView imageRecipe_iv;
    private TextView name_tv;
    private FlexboxLayout tagsChipGroup;
    private LinearLayout ingredientsList_lv;
    private LinearLayout procedureLayout;
    private String recipeId;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        MobileAds.initialize(this, initializationStatus -> {
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        Intent intent = getIntent();
        recipeId = intent.getStringExtra("RECIPE_ID");
        presenter = new RecipePresenter(this);
        initComponents();
        triggerGetRecipeDetails(recipeId);
    }

    private void initComponents() {
        imageRecipe_iv = findViewById(R.id.imageRecipeDetail);
        name_tv = findViewById(R.id.name_tv);
        tagsChipGroup = findViewById(R.id.tagsRecipesDetailsChips);
        ingredientsList_lv = findViewById(R.id.ingredientsList_lv);
        procedureLayout = findViewById(R.id.procedureLayout);
    }

    @Override
    public void triggerGetRecipeDetails(String recipeId) {
        presenter.requestRecipeDetails(recipeId);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setRecipesRetails(RecipeDetail recipeDetails) {
        Picasso.with(this).load("https://s3.amazonaws.com/images.mykitchen.com.mx/recipes/"+recipeDetails.getImage())
                .into(imageRecipe_iv);
        name_tv.setText(recipeDetails.getName());

        for (TagsList tag: recipeDetails.getTags()){
            Chip chip = new Chip(this);
            chip.setText(tag.getTag().getName());
            chip.setClickable(false);
            tagsChipGroup.addView(chip);
        }

        for (Ingredient ingredient : recipeDetails.getGroup().get(0).getIngredients()){
            Chip chip = new Chip(this);
            if (ingredient.getUnitId() != null){
                if(ingredient.getUnitId().equals(String.valueOf(Units.gramos.ordinal()))){
                    chip.setText(ingredient.getQuantity() + " " + Units.grs.name() + " de "+ ingredient.getProduct().getName());
                }else if (ingredient.getUnitId().equals(String.valueOf(Units.piezas.ordinal()))){
                    chip.setText(ingredient.getQuantity() + " "+ ingredient.getProduct().getName());
                }else if (ingredient.getUnitId().equals(String.valueOf(Units.mililitros.ordinal()))){
                    chip.setText(ingredient.getQuantity() + " " + Units.mls.name() + " "+ ingredient.getProduct().getName());
                }

            }else{
                chip.setText(ingredient.getProduct().getName());
            }

            chip.setClickable(false);
            chip.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            chip.setChipIcon(getDrawable(R.drawable.ic_kitchen_icon));
            ingredientsList_lv.addView(chip);
        }

        for (Procedure procedure : recipeDetails.getProcedure()){
            TextView title = new TextView(this);
            TextView step = new TextView(this);
            title.setText("Paso " + procedure.getStepNumber() + ":");
            step.setText(procedure.getDescription());
            Paris.style(title).apply(R.style.stepTitle);
            Paris.style(step).apply(R.style.stepBody);
            procedureLayout.addView(title);
            procedureLayout.addView(step);
        }

    }

}
