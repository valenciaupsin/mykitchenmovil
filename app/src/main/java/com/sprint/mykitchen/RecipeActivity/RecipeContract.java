package com.sprint.mykitchen.RecipeActivity;

import com.sprint.mykitchen.RecipeActivity.model.RecipeDetail;
import com.sprint.mykitchen.APIServices.response.RecipeDetailResponse;

import retrofit2.Response;

public interface RecipeContract {
    interface View{
        void triggerGetRecipeDetails(String recipeId);

        void setRecipesRetails(RecipeDetail recipeDetails);
    }
    interface Presenter{
        void requestRecipeDetails(String recipeId);

        void triggerSetRecipeDetails(Response<RecipeDetailResponse> response);

    }
    interface Interactor{
        void getRecipeDetail(String recipeId);

    }
    interface Router{

    }
}
