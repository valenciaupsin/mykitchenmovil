package com.sprint.mykitchen.inventory;

import com.google.gson.Gson;
import com.sprint.mykitchen.APIServices.APIAdapter;
import com.sprint.mykitchen.APIServices.response.GenericResponse;
import com.sprint.mykitchen.APIServices.response.ProductCategoryResponse;
import com.sprint.mykitchen.APIServices.response.StockResponse;
import com.sprint.mykitchen.inventory.model.NewProduct;
import com.sprint.mykitchen.inventory.model.Quantity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryInteractor implements InventoryContract.Interactor {
    private InventoryContract.Presenter presenter;
    private String public_token = "S<yus%;|ZO'1k/ISa^H+6_,!:&$0Z+kM9)B?;f`=]=p%q!)uJ^x_!F!7!LL&F|B";

    public InventoryInteractor(InventoryPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void requestProductsStoke(String TOKEN, String KITCHENID, String ACCOUNTID) {
        Thread thread = new Thread(() -> {
            Call<StockResponse> call = APIAdapter.getApiService2(TOKEN).getProductStoke(TOKEN,ACCOUNTID,KITCHENID);
            call.enqueue(new Callback<StockResponse>() {
                @Override
                public void onResponse(Call<StockResponse> call, Response<StockResponse> response) {
                    if (response.isSuccessful()){
                        presenter.triggerSetProductsStoke(response);
                    }
                }

                @Override
                public void onFailure(Call<StockResponse> call, Throwable t) {
                    presenter.ResponseError();
                }
            });
        });
        thread.start();
    }

    @Override
    public void requestProductsToAdd() {
        Thread thread = new Thread(() -> {
            Call<ProductCategoryResponse> call = APIAdapter.getApiService(public_token).getProductsCategoriesComplete(public_token);
            call.enqueue(new Callback<ProductCategoryResponse>() {
                @Override
                public void onResponse(Call<ProductCategoryResponse> call, Response<ProductCategoryResponse> response) {
                    switch (response.code()){
                        case 200: presenter.triggerSetProductsToAdd(response.body());break;
                    }
                }

                @Override
                public void onFailure(Call<ProductCategoryResponse> call, Throwable t) {
                    presenter.ResponseError();
                }
            });

        });
        thread.start();
    }

    String toJSON(ArrayList<NewProduct> list) {
        Gson gson = new Gson();
        StringBuilder sb = new StringBuilder();
        for(NewProduct d : list) {
            sb.append(gson.toJson(d));
        }
        return sb.toString();
    }
    @Override
    public void AddProduct(String TOKEN, String KITCHENID, String ACCOUNTID, NewProduct product) {
        Thread thread = new Thread(() -> {
            Call<GenericResponse> call = APIAdapter.getApiService2(TOKEN).addStock(ACCOUNTID,KITCHENID,product);
            call.enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                    if (response.isSuccessful()){
                        presenter.triggerSetMessageResponse(response.body().getDetails());
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    presenter.ResponseError();
                }
            });
        });
        thread.start();
    }

    @Override
    public void ModifyProduct(String TOKEN, String KITCHENID, String ACCOUNTID, String stock_id, int quantityy) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Quantity quantity = new Quantity(String.valueOf(quantityy));
                Call<GenericResponse> call = APIAdapter.getApiService2(TOKEN).putStock(ACCOUNTID,KITCHENID, stock_id, quantity);
                call.enqueue(new Callback<GenericResponse>() {
                    @Override
                    public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                        if (response.isSuccessful()){
                            presenter.triggerSetMessageResponse(response.body().getDetails());
                        }
                    }

                    @Override
                    public void onFailure(Call<GenericResponse> call, Throwable t) {
                        presenter.ResponseError();
                    }
                });
            }
        });
        thread.start();
    }
}
