package com.sprint.mykitchen.inventory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sprint.mykitchen.R;
import com.sprint.mykitchen.ExplorarActivity.model.Product;
import com.sprint.mykitchen.constants.Units;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AddProductAdapter extends RecyclerView.Adapter<AddProductAdapter.ViewHolder>  {
    private Context context;
    private ArrayList<Product> productArrayList;
//    private View.OnClickListener listener;
    private OnItemClickListener nListener;

    public AddProductAdapter(Context context, ArrayList<Product> productArrayList) {
        this.context = context;
        this.productArrayList = productArrayList;
    }

    public interface  OnItemClickListener{
//        void onItemClick(int position);
        void onMinusClick(int position);
        void onPlusClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        nListener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingred_item, null,false);
//        view.setOnClickListener(this);
        return new ViewHolder(view, nListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context).load("https://s3.amazonaws.com/images.mykitchen.com.mx/products/"+productArrayList.get(position).getImage())
                .into(holder.imageView);
        holder.name.setText(productArrayList.get(position).getName()+ " "+ productArrayList.get(position).getSubproducts().get(0).getPresentation());
        holder.quantity.setText(String.valueOf(productArrayList.get(position).getSubproducts().get(0).getQuantity()));
        holder.unit.setText(getUnitText(productArrayList.get(position).getSubproducts().get(0).getStepUnitId()));
        holder.menosBtn.setVisibility(View.VISIBLE);
        holder.masBtn.setVisibility(View.VISIBLE);
        holder.deleteBtn.setVisibility(View.GONE);
    }

    private String getUnitText(String stepUnitId) {
        switch (stepUnitId){
            case "1": return Units.grs.name();
            case "2": return Units.pzs.name();
            case "3": return Units.mls.name();
            default: return "";
        }
    }


    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;
        LinearLayout buttonsLayot;
        ImageButton menosBtn;
        TextView quantity;
        ImageButton masBtn;
        TextView unit;
        ImageButton deleteBtn;

        public ViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageIngre_iv);
            name = itemView.findViewById(R.id.nombreIngre_tv);
            buttonsLayot = itemView.findViewById(R.id.quantity_btns);
            menosBtn = itemView.findViewById(R.id.menosIngre_btn);
            quantity = itemView.findViewById(R.id.cantidadIngre_tv);
            masBtn = itemView.findViewById(R.id.masIngre_btn);
            deleteBtn = itemView.findViewById(R.id.deleteIng_btn);
            unit = itemView.findViewById(R.id.unidadIngre_tv);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });*/
            menosBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onMinusClick(position);
                        }
                    }
                }
            });
            masBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onPlusClick(position);
                        }
                    }
                }
            });
        }
    }
}
