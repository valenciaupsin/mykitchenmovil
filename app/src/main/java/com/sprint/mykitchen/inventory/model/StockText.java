package com.sprint.mykitchen.inventory.model;

public class StockText {
    private String id=null;
    private String productId=null;
    private String subproductId=null;
    private int quantity=0;


    public String getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public String getSubproductId() {
        return subproductId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void plus(int quantity){
        this.quantity += quantity;
    }

    public void minus(int quantity){
        if (this.quantity < quantity)
            this.quantity = 0;
        else
            this.quantity -= quantity;
    }

    public void delete(){
        this.quantity = 0;
    }
}
