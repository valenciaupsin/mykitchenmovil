package com.sprint.mykitchen.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sprint.mykitchen.R;
import com.sprint.mykitchen.ExplorarActivity.model.Product;
import com.sprint.mykitchen.inventory.model.NewProduct;
import com.sprint.mykitchen.inventory.model.NewStockValues;
import com.sprint.mykitchen.inventory.model.Stock;
import com.sprint.mykitchen.inventory.model.operations;

import java.util.ArrayList;

public class InventoryActivity extends AppCompatActivity implements InventoryContract.View{

    private InventoryContract.Presenter presenter;

    private RecyclerView.LayoutManager layoutManager;


    private RecyclerView productsRV;
    private Button doActionBtn;
    private View empty_inventory;
    private ProgressBar progressBar_inventory;
    private FloatingActionButton new_product_btn;
    private FloatingActionButton delete_product_btn;
    private FloatingActionButton modify_product_btn;
    private ImageButton back_btn;

    private String TOKEN;
    private String KITCHENID;
    private String ACCOUNTID;
    private operations OPERATION;

    private ArrayList<Stock> stockArrayList;
    private ArrayList<Product> allProducts;

    ArrayList<String> modifiedStockPositions = new ArrayList<>();
    ArrayList<String> addedSubroductId = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
        initComponents();
        loadAccountData();
        loadState();
        presenter.triggerRequestProductsStoke(TOKEN, KITCHENID, ACCOUNTID);
    }

    private void initComponents(){
        presenter = new InventoryPresenter(this);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(this::onClick);
        empty_inventory = findViewById(R.id.empty_inventory);
        empty_inventory.setVisibility(View.GONE);
        progressBar_inventory = findViewById(R.id.progressBar_inventory);
        productsRV = findViewById(R.id.products_lv);
        productsRV.setVisibility(View.GONE);
        doActionBtn = findViewById(R.id.doAction_btn);
        doActionBtn.setVisibility(View.GONE);
        doActionBtn.setOnClickListener(this::onClick);
        new_product_btn = findViewById(R.id.new_product_btn);
        delete_product_btn = findViewById(R.id.delete_product_btn);
        modify_product_btn = findViewById(R.id.modify_product_btn);
        new_product_btn.setOnClickListener(this::onClick);
        delete_product_btn.setOnClickListener(this::onClick);
        modify_product_btn.setOnClickListener(this::onClick);
        new_product_btn.setVisibility(View.GONE);
        delete_product_btn.setVisibility(View.GONE);
        modify_product_btn.setVisibility(View.GONE);

    }

    private void loadAccountData(){
        Intent intent = getIntent();
        TOKEN = intent.getStringExtra("TOKEN");
        KITCHENID = intent.getStringExtra("KITCHENID");
        ACCOUNTID = intent.getStringExtra("ACCOUNTID");
    }

    private void onClick(View view) {
        if (view == back_btn){
            finish();
            setResult(RESULT_OK);
        }else if(view == new_product_btn){
            presenter.triggerRequestProductsToAdd();
        }else if (view == delete_product_btn){
            deleteProducts();
        }else if (view == modify_product_btn){
            modifyProducts();
        }else if (view == doActionBtn){
            if (OPERATION == operations.add){
                getAddedStock();
            }else if (OPERATION ==operations.modify || OPERATION == operations.delete){
                getModifiedStocks();
            }
        }
    }

    private void getAddedStock(){
        ArrayList<NewProduct> newProductsValues = new ArrayList<>();
        for (Product product: allProducts){
            for (String added: addedSubroductId){
                if (product.getSubproducts().get(0).getId().equals(added) && product.getSubproducts().get(0).getQuantity() > 0){
                    newProductsValues.add(new NewProduct(product.getSubproducts().get(0).getProductId(),product.getSubproducts().get(0).getId(),product.getSubproducts().get(0).getQuantity()));
                    break;
                }
            }
        }

        presenter.triggerAddProduct(TOKEN, KITCHENID, ACCOUNTID, newProductsValues);
        addedSubroductId.clear();
    }

    private void getModifiedStocks() {
        ArrayList<NewStockValues> newStockValues = new ArrayList<>();
        if (OPERATION == operations.modify){
            for (Stock stoke: stockArrayList){
                for (String modified: modifiedStockPositions){
                    if (stoke.getId().equals(modified))
                        newStockValues.add(new NewStockValues(stoke.getId(),stoke.getQuantity()));
                }
            }
        }else if (OPERATION == operations.delete) {
            for (String modified: modifiedStockPositions){
                newStockValues.add(new NewStockValues(modified,0));
            }
        }
        presenter.triggerModifyProduct(newStockValues, TOKEN, KITCHENID, ACCOUNTID);
        modifiedStockPositions.clear();
    }

    @Override
    public void setProductsStoke(ArrayList<Stock> content) {
        progressBar_inventory.setVisibility(View.GONE);
        empty_inventory.setVisibility(View.GONE);
        productsRV.setVisibility(View.VISIBLE);
        new_product_btn.setVisibility(View.VISIBLE);
        delete_product_btn.setVisibility(View.VISIBLE);
        modify_product_btn.setVisibility(View.VISIBLE);
        doActionBtn.setVisibility(View.GONE);

        OPERATION = operations.watch;

        stockArrayList = content;
        layoutManager = new LinearLayoutManager(this);
        productsRV.setLayoutManager(layoutManager);
        ListViewAdapter listViewAdapter = new ListViewAdapter(this, content);
        productsRV.setAdapter(listViewAdapter);
    }

    @Override
    public void setProductsToAdd(ArrayList<Product> products) {
        allProducts = products;
        empty_inventory.setVisibility(View.GONE);
        productsRV.setVisibility(View.VISIBLE);
        new_product_btn.setVisibility(View.GONE);
        delete_product_btn.setVisibility(View.GONE);
        modify_product_btn.setVisibility(View.GONE);
        doActionBtn.setVisibility(View.VISIBLE);
        doActionBtn.setText("Agregar al inventario");

        OPERATION = operations.add;

        layoutManager = new LinearLayoutManager(this);
        productsRV.setLayoutManager(layoutManager);
        AddProductAdapter addProductAdapter = new AddProductAdapter(this, allProducts);
        productsRV.setAdapter(addProductAdapter);

        addProductAdapter.setOnItemClickListener(new AddProductAdapter.OnItemClickListener() {
            @Override
            public void onMinusClick(int position) {
                allProducts.get(position).getSubproducts().get(0).minus(allProducts.get(position).getSubproducts().get(0).getStep());
                addProductAdapter.notifyItemChanged(position);
                if (!addedSubroductId.contains(allProducts.get(position).getSubproducts().get(0).getId())){
                    addedSubroductId.add(allProducts.get(position).getSubproducts().get(0).getId());
                }
            }

            @Override
            public void onPlusClick(int position) {
                allProducts.get(position).getSubproducts().get(0).plus(allProducts.get(position).getSubproducts().get(0).getStep());
                addProductAdapter.notifyItemChanged(position);
                if (!addedSubroductId.contains(allProducts.get(position).getSubproducts().get(0).getId())){
                    addedSubroductId.add(allProducts.get(position).getSubproducts().get(0).getId());
                }
            }
        });
    }

    private void modifyProducts() {
        new_product_btn.setVisibility(View.GONE);
        modify_product_btn.setVisibility(View.GONE);
        delete_product_btn.setVisibility(View.GONE);
        productsRV.setVisibility(View.VISIBLE);
        doActionBtn.setVisibility(View.VISIBLE);
        doActionBtn.setText("Modificar inventario");

        OPERATION = operations.modify;

        layoutManager = new LinearLayoutManager(this);
        productsRV.setLayoutManager(layoutManager);
        ModifyProductAdapter modifyProductAdapter = new ModifyProductAdapter(this, stockArrayList);
        productsRV.setAdapter(modifyProductAdapter);
        modifyProductAdapter.setOnItemClickListener(new ModifyProductAdapter.OnItemClickListener() {
            @Override
            public void onMinusClick(int position) {
                stockArrayList.get(position).minus(stockArrayList.get(position).getSubproductId().getStep());
                modifyProductAdapter.notifyItemChanged(position);
                if (!modifiedStockPositions.contains(stockArrayList.get(position).getId())){
                    modifiedStockPositions.add(stockArrayList.get(position).getId());
                }
            }

            @Override
            public void onPlusClick(int position) {
                stockArrayList.get(position).plus(stockArrayList.get(position).getSubproductId().getStep());
                modifyProductAdapter.notifyItemChanged(position);
                if (!modifiedStockPositions.contains(stockArrayList.get(position).getId())){
                    modifiedStockPositions.add(stockArrayList.get(position).getId());
                }
            }
        });
    }

    private void deleteProducts() {
        new_product_btn.setVisibility(View.INVISIBLE);
        modify_product_btn.setVisibility(View.INVISIBLE);
        delete_product_btn.setVisibility(View.INVISIBLE);
        productsRV.setVisibility(View.VISIBLE);
        doActionBtn.setVisibility(View.VISIBLE);
        doActionBtn.setText("Aceptar");

        OPERATION = operations.delete;

        layoutManager = new LinearLayoutManager(this);
        productsRV.setLayoutManager(layoutManager);
        DeleteProductAdapter deleteProductAdapter = new DeleteProductAdapter(this, stockArrayList);
        productsRV.setAdapter(deleteProductAdapter);

        deleteProductAdapter.setOnItemClickListener(new DeleteProductAdapter.OnItemClickListener() {
            @Override
            public void onDeleteClick(int position) {
                stockArrayList.get(position).delete();
                modifiedStockPositions.add(stockArrayList.get(position).getId());
                stockArrayList.remove(position);
                deleteProductAdapter.notifyItemRemoved(position);
                /*if (!modifiedStockPositions.contains(stockArrayList.get(position).getId())){
                }*/
            }
        });
    }

    @Override
    public void setSuccesModify(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setError() {
        Toast.makeText(this, "Error al conectar con el servidor, intente más tarde", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void emptyStock() {
        empty_inventory.setVisibility(View.VISIBLE);
        progressBar_inventory.setVisibility(View.GONE);
        productsRV.setVisibility(View.GONE);
        doActionBtn.setVisibility(View.GONE);
        new_product_btn.setVisibility(View.VISIBLE);
        modify_product_btn.setVisibility(View.INVISIBLE);
        delete_product_btn.setVisibility(View.INVISIBLE);
        OPERATION = operations.watch;
    }

    private void loadState(){
        progressBar_inventory.setVisibility(View.VISIBLE);
        productsRV.setVisibility(View.GONE);
        doActionBtn.setVisibility(View.GONE);
        new_product_btn.setVisibility(View.GONE);
        modify_product_btn.setVisibility(View.GONE);
        delete_product_btn.setVisibility(View.GONE);
        empty_inventory.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        switch (OPERATION){
            case watch: onClick(back_btn);break;
            case add:
            case delete:
            case modify:{
                loadState();
                modifiedStockPositions.clear();
                addedSubroductId.clear();
                presenter.triggerRequestProductsStoke(TOKEN, KITCHENID, ACCOUNTID);
            }break;
        }

    }
}
