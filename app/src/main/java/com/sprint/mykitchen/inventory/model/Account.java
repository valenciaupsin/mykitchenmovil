package com.sprint.mykitchen.inventory.model;

import java.util.ArrayList;

public class Account {
    private String id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String accountId;
    private ArrayList<Stock> stock;

    public String getId() {
        return id;
    }

    public String getAccountId() {
        return accountId;
    }

    public ArrayList<Stock> getStock() {
        return stock;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

}
