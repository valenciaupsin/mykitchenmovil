package com.sprint.mykitchen.inventory;

import com.sprint.mykitchen.APIServices.response.ProductCategoryResponse;
import com.sprint.mykitchen.APIServices.response.StockResponse;
import com.sprint.mykitchen.ExplorarActivity.model.ProductCategory;
import com.sprint.mykitchen.ExplorarActivity.model.Product;
import com.sprint.mykitchen.inventory.model.NewProduct;
import com.sprint.mykitchen.inventory.model.NewStockValues;

import java.util.ArrayList;

import retrofit2.Response;

public class InventoryPresenter implements InventoryContract.Presenter {
    private InventoryContract.View view;
    private InventoryContract.Interactor interactor;

    public InventoryPresenter(InventoryActivity view) {
        this.view = view;
        this.interactor = new InventoryInteractor(this);
    }

    @Override
    public void triggerRequestProductsStoke(String TOKEN, String KITCHENID, String ACCOUNTID) {
        interactor.requestProductsStoke(TOKEN, KITCHENID, ACCOUNTID);
    }

    @Override
    public void triggerRequestProductsToAdd() {
        interactor.requestProductsToAdd();
    }

    @Override
    public void triggerSetProductsStoke(Response<StockResponse> stockResponse) {
        if (stockResponse.body().getContent() !=null)
            view.setProductsStoke(stockResponse.body().getContent());
        else
            view.emptyStock();
    }

    @Override
    public void triggerAddProduct(String TOKEN, String KITCHENID, String ACCOUNTID, ArrayList<NewProduct> products) {
//        interactor.AddProduct(TOKEN, KITCHENID, ACCOUNTID, products);
        for (NewProduct newProduct: products){
            interactor.AddProduct(TOKEN, KITCHENID, ACCOUNTID, newProduct);
        }
    }

    @Override
    public void triggerModifyProduct(ArrayList<NewStockValues> newStockValues, String TOKEN, String KITCHENID, String ACCOUNTID) {
        for (NewStockValues stockValues: newStockValues){
            interactor.ModifyProduct(TOKEN, KITCHENID, ACCOUNTID, stockValues.getStock_id(),stockValues.getQuantity());
        }
    }

    @Override
    public void triggerSetMessageResponse(String message) {
        view.setSuccesModify(message);
    }

    @Override
    public void triggerSetProductsToAdd(ProductCategoryResponse body) {
        ArrayList<ProductCategory> productCategories = body.getContent();
        ArrayList<Product> productsList = new ArrayList<>();

        for (ProductCategory category: productCategories){
            productsList.addAll(category.getProduct());
        }
        view.setProductsToAdd(productsList);
    }

    @Override
    public void ResponseError() {
        view.setError();
    }
}
