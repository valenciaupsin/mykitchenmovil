package com.sprint.mykitchen.inventory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sprint.mykitchen.R;
import com.sprint.mykitchen.inventory.model.Stock;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ListViewAdapter extends RecyclerView.Adapter<ListViewAdapter.ViewHolder> implements View.OnClickListener {
    private Context context;
    private ArrayList<Stock> productArrayList;
    private View.OnClickListener listener;

    public ListViewAdapter(Context context, ArrayList<Stock> productArrayList) {
        this.context = context;
        this.productArrayList = productArrayList;
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;

    }
    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingred_item, null,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context).load("https://s3.amazonaws.com/images.mykitchen.com.mx/products/"+productArrayList.get(position).getProductId().getImage())
                .into(holder.imageView);
//        holder.imageView.setVisibility(View.INVISIBLE);
        holder.name.setText(productArrayList.get(position).getProductId().getName()+ " "+ productArrayList.get(position).getSubproductId().getPresentation());
        holder.quantity.setText(String.valueOf(productArrayList.get(position).getQuantity()));
        holder.quantity.setVisibility(View.VISIBLE);
        holder.unit.setText(getUnitText(productArrayList.get(position).getSubproductId().getUnitId()));
        holder.menosBtn.setVisibility(View.INVISIBLE);
        holder.masBtn.setVisibility(View.INVISIBLE);
        holder.deleteBtn.setVisibility(View.GONE);

    }

    private String getUnitText(String unitId) {
        switch (Integer.parseInt(unitId)){
            case 1: return "grs";
            case 2: return "pzs";
            case 3: return "mls";
            default: return "";

        }
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;
        LinearLayout buttonsLayot;
        ImageButton menosBtn;
        TextView quantity;
        ImageButton masBtn;
        TextView unit;
        ImageButton deleteBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageIngre_iv);
            name = itemView.findViewById(R.id.nombreIngre_tv);
            buttonsLayot = itemView.findViewById(R.id.quantity_btns);
            menosBtn = itemView.findViewById(R.id.menosIngre_btn);
            quantity = itemView.findViewById(R.id.cantidadIngre_tv);
            masBtn = itemView.findViewById(R.id.masIngre_btn);
            deleteBtn = itemView.findViewById(R.id.deleteIng_btn);
            unit = itemView.findViewById(R.id.unidadIngre_tv);
        }
    }
}
