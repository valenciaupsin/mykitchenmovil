package com.sprint.mykitchen.inventory.model;

import com.sprint.mykitchen.ExplorarActivity.model.Product;
import com.sprint.mykitchen.ExplorarActivity.model.Subproduct;

public class Stock {
    private String id;
    private Product productId;
    private Subproduct subproductId;
    private int quantity;

    public Stock(String id, Product productId, Subproduct subproductId, int quantity) {
        this.id = id;
        this.productId = productId;
        this.subproductId = subproductId;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public Product getProductId() {
        return productId;
    }

    public Subproduct getSubproductId() {
        return subproductId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void plus(int quantity){
        this.quantity += quantity;
    }

    public void minus(int quantity){
        if (this.quantity < quantity)
            this.quantity = 0;
        else
            this.quantity -= quantity;
    }

    public void delete(){
        this.quantity = 0;
    }
}
