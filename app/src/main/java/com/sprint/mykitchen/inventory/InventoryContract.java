package com.sprint.mykitchen.inventory;

import com.sprint.mykitchen.APIServices.response.ProductCategoryResponse;
import com.sprint.mykitchen.APIServices.response.StockResponse;
import com.sprint.mykitchen.ExplorarActivity.model.Product;
import com.sprint.mykitchen.inventory.model.NewProduct;
import com.sprint.mykitchen.inventory.model.NewStockValues;
import com.sprint.mykitchen.inventory.model.Stock;

import java.util.ArrayList;

import retrofit2.Response;

public interface InventoryContract {

    interface View{

        void setProductsStoke(ArrayList<Stock> content);

        void setProductsToAdd(ArrayList<Product> products);

        void emptyStock();

        void setSuccesModify(String message);

        void setError();
    }

    interface Presenter{

        void triggerRequestProductsStoke(String TOKEN, String KITCHENID, String ACCOUNTID);

        void triggerAddProduct(String TOKEN, String KITCHENID, String ACCOUNTID, ArrayList<NewProduct> products);

        void triggerModifyProduct(ArrayList<NewStockValues> newStockValues, String TOKEN, String KITCHENID, String ACCOUNTID);

        void triggerSetMessageResponse(String message);

        void triggerSetProductsStoke(Response<StockResponse> stockResponse);

        void triggerRequestProductsToAdd();

        void ResponseError();

        void triggerSetProductsToAdd(ProductCategoryResponse body);
    }

    interface Interactor{

        void requestProductsStoke(String TOKEN, String KITCHENID, String ACCOUNTID);

        void AddProduct(String TOKEN, String KITCHENID, String ACCOUNTID, NewProduct product);

        void ModifyProduct(String TOKEN, String KITCHENID, String ACCOUNTID, String stock_id, int quantity);

        void requestProductsToAdd();
    }
}
