package com.sprint.mykitchen.inventory.model;

public class NewStockValues {
    private String stock_id;
    private int quantity;

    public NewStockValues(String stock_id, int quantity) {
        this.stock_id = stock_id;
        this.quantity = quantity;
    }

    public String getStock_id() {
        return stock_id;
    }

    public int getQuantity() {
        return quantity;
    }
}
