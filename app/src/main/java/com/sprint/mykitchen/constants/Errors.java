package com.sprint.mykitchen.constants;

public enum Errors {
    DataError,
    AutorizationError
}
