package com.sprint.mykitchen.constants;

public enum DificultyID {
    Facil,
    Intermedio,
    Difícil
}
