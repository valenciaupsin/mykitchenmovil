package com.sprint.mykitchen.constants;

public enum Units {
    error,
    gramos,
    piezas,
    mililitros,
    grs,
    pzs,
    mls
}
